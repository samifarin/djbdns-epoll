To build release version:
```
rm -rf build && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make -j4
```
To build debug version:
```
rm -rf build && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Debug .. && make -j4
```
To change compiler:
```
CXX=clang++ CC=clang cmake ..
```

Installing the software is disabled in Makefile.
For now, only dnscache is upgraded to epoll.
Only Linux with epoll support is supported.

okclient-feature in dnscache is nuked, so use iptables to configure
client access (otherwise anybody can query your DNS proxy server).

timeouts have been changed.  If you do not like that, try
```
ctags -R && vim -t timeouts
```

If your virgin dnscache uses 0.01% CPU time (at max),
you won't notice if it uses now 0.001% instead.
So don't whine to me that this patch does not help you.

Try disabling selinux for extra performance.

Make env/DATALIMIT bigger than you usually do because
bss alone takes 11 MB with MAXUDP=10000 + MAXTCP=1000.

To install:
```
./dnscache-conf dnscache dnslog /var/dnscache-epoll 127.0.0.42
```

Assumes ```dnsroots.global``` is at ```/etc```.
Then install dnscache into e.g. /usr/local/bin/dnscache-epoll.
then edit /var/dnscache-epoll/run to use correct binary
(e.g. /usr/local/bin/dnscache-epoll).

Then configure svscan so that it starts up dnscache-epoll service.
After you have tested that it works ok, change IP address to
127.0.0.1, terminate old dnscache, and have some more fun.

Entropy is read calling getrandom or reading /dev/urandom.

Quiet mode is turned on at startup.
For verbose operation, run
```
svc -1 /service/dnscache-epoll
```
and for quiet operation, run
```
svc -2 /service/dnscache-epoll
```

Downloads are at
https://gitlab.com/samifarin/djbdns-epoll

Note To Self:
Flow of a UDP query:
```
u_new -> query_start -> cleanup -> doit -> dns_transmit_start -> firstudp -> thisudp -> prepquery/try_merge
```
When query is merged, in logs there is "merge" instead of "query".

To specify the same formatting in vim that I use, add to your .vimrc:
auto BufNewfile,BufEnter /usr/local/src/djbdns-1.05-git/* set shiftwidth=2 softtabstop=2 expandtab cindent

```
CPU usage, %, ignoring kernel
doit                   17
dns_packet_getname     12
cache_get              11
malloc/free             9
memcpy/memset/memcmp    6
siphash                 5
...
chacha                  1
```

One CPU (SandyBridge) can handle about 9000 queries/second from clients (forwardonly disabled)
with 2000 concurrent queries—including kernel usage.  No dnscurve, logging or TCP clients in
this measurement...
This is approx 4.5 times faster than virgin dnscache (it had logging enabled).
