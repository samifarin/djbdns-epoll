#include "base32.h"
#include "byte.h"

const char base32_digits[32] = "0123456789bcdfghjklmnpqrstuvwxyz";

void base32_encodebytes(char *out, const char *in, size_t len)
{
  unsigned int i, x, v, vbits;

  x = v = vbits = 0;
  for (i = 0; i < len; ++i) {
    v |= ((unsigned int) (unsigned char) in[i]) << vbits;
    vbits += 8;
    do {
      out[++x] = base32_digits[v & 31];
      v >>= 5;
      vbits -= 5;
      if (x == 50) {
        *out = x;
        out += 1 + x;
        x = 0;
      }
    } while (vbits >= 5);
  }

  if (vbits) out[++x] = base32_digits[v & 31];
  if (x) *out = x;
}

void base32_encodekey(char *out, const char *key)
{
  unsigned int i, v, vbits;

  byte_copy(out, 4, "\66x1a");
  out += 4;

  v = vbits = 0;
  for (i = 0; i < 32; ++i) {
    v |= ((unsigned int) (unsigned char) key[i]) << vbits;
    vbits += 8;
    do {
      *out++ = base32_digits[v & 31];
      v >>= 5;
      vbits -= 5;
    } while (vbits >= 5);
  }
}

unsigned int base32_decode(char *out,const char *in,size_t len,int mode)
{
  /*
   * digits = '0123456789bcdfghjklmnpqrstuvwxyz'
   * ','.join('%2d' % digits.find(chr(x).lower()) for x in xrange(256))
   */
  static const char val[256] = {
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     0, 1, 2, 3, 4, 5, 6, 7, 8, 9,-1,-1,-1,-1,-1,-1,
    -1,-1,10,11,12,-1,13,14,15,-1,16,17,18,19,20,-1,
    21,22,23,24,25,26,27,28,29,30,31,-1,-1,-1,-1,-1,
    -1,-1,10,11,12,-1,13,14,15,-1,16,17,18,19,20,-1,
    21,22,23,24,25,26,27,28,29,30,31,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  };

  unsigned int i;
  unsigned int x, v, vbits;
  char *out0 = out;

  v = vbits = 0;
  for (i = 0; i < len; ++i) {
    x = (unsigned char) val[(unsigned char) in[i]];
    if (x >= 32) return 0;
    v |= x << vbits;
    vbits += 5;
    if (vbits >= 8) {
      *out++ = v;
      v >>= 8;
      vbits -= 8;
    }
  }

  if (mode) {
    if (vbits)
      *out++ = v;
  }
  else if (vbits >= 5 || v)
    return 0;

  return out - out0;
}
