#ifndef BYTE_H
#define BYTE_H

#include <string.h>

#include "helper.h"

extern unsigned long int byte_chr(const void*, unsigned long int, int);
extern unsigned long int byte_rchr(const void*, unsigned long int, int);

#define byte_copy(t,n,f) __builtin_memcpy((t),(f),(n))
#define byte_copyr(t,n,f) __builtin_memmove((t),(f),(n))
#define byte_zero(t,n) __builtin_memset(t, 0, n)

static __always_inline int __constant_byte_diff(const char *s, unsigned long int n, const char *t)
{
  for (;;) {
    if (!n) return 0; if (*s != *t) break; ++s; ++t; --n;
    if (!n) return 0; if (*s != *t) break; ++s; ++t; --n;
    if (!n) return 0; if (*s != *t) break; ++s; ++t; --n;
    if (!n) return 0; if (*s != *t) break; ++s; ++t; --n;
  }
  return ((int)(unsigned int)(unsigned char) *s)
       - ((int)(unsigned int)(unsigned char) *t);
}

#define byte_diff(t,n,f) \
  (__builtin_constant_p(n) ? \
     __constant_byte_diff(t,n,f) : \
     __builtin_memcmp(t, f, n))
#define byte_equal(s,n,t) (!byte_diff((s),(n),(t)))

#endif
