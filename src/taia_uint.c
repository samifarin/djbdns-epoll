#include "taia.h"

/* XXX: breaks tai encapsulation */

void taia_uint(struct taia *t,unsigned int s)
{
  t->sec.x = s;
  t->nano = 0;
  t->atto = 0;
}

void taia_nano(struct taia *t,uint64 ns)
{
  t->sec.x = 0;
  t->nano = ns;
  t->atto = 0;
}
