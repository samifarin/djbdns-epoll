#include <inttypes.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sodium.h>

#include "socket.h"
#include "alloc.h"
#include "error.h"
#include "byte.h"
#include "ip6.h"
#include "dns.h"
#include "strerr.h"
#include "log.h"
#include "case.h"
#include "base32.h"
#include "siphash.h"

#define crypto_box_afternm crypto_box_curve25519xsalsa20poly1305_afternm
#define crypto_box_open_afternm crypto_box_curve25519xsalsa20poly1305_open_afternm

int merge_enable;
uint64 curveok;
uint64 curvefail;
uint64 depths[5];

static void (*merge_logger)(const char *, const char *, const char *);

static void makebasequery(struct dns_transmit *d, char *query)
{
  unsigned int len;

  len = dns_domain_length(d->name);

  byte_copy(query, 2, d->nonce + 8);
  byte_copy(query + 2, 10, d->fl.flagrecursive ? "\1\0\0\1\0\0\0\0\0\0" : "\0\0\0\1\0\0\0\0\0\0");
  byte_copy(query + 12, len, d->name);
  byte_copy(query + 12 + len, 2, d->qtype);
  byte_copy(query + 14 + len, 2, DNS_C_IN);
}

static void prepquery(struct dns_transmit *d)
{
  unsigned int len;
  char nonce[24];
  const char *key;
  unsigned int m;
  unsigned int suffixlen;

  charcrandom_buf(d->nonce, sizeof(d->nonce));

  if (!d->keys) {
    byte_copy(d->query + 2, 2, d->nonce + 8);
    return;
  }

  len = dns_domain_length(d->name);

  byte_copy(nonce, 12, d->nonce);
  byte_zero(nonce + 12, 12);
  key = d->keys + 32 * d->curserver;

  byte_zero(d->query, 32);
  makebasequery(d, d->query + 32);
  crypto_box_afternm((unsigned char *) d->query,
                     (const unsigned char *) d->query,
                     len + 48,
                     (const unsigned char *) nonce,
                     (const unsigned char *) key);

  if (!d->suffix) {
    byte_copyr(d->query + 54, len + 32, d->query + 16);
    uint16_pack_big(d->query, len + 84);
    byte_copy(d->query + 2, 8, "Q6fnvWj8");
    byte_copy(d->query + 10, 32, d->pubkey);
    byte_copy(d->query + 42, 12, nonce);
    return;
  }

  byte_copyr(d->query + d->querylen - len - 32, len + 32, d->query + 16);
  byte_copy(d->query + d->querylen - len - 44, 12, nonce);

  suffixlen = dns_domain_length(d->suffix);
  m = base32_bytessize(len + 44);

  uint16_pack_big(d->query, d->querylen - 2);
  charcrandom_buf(&d->query[2], 2);
  byte_copy(d->query + 4, 10, "\0\0\0\1\0\0\0\0\0\0");
  base32_encodebytes(d->query + 14, d->query + d->querylen - len - 44, len + 44);
  base32_encodekey(d->query + 14 + m, d->pubkey);
  byte_copy(d->query + 69 + m, suffixlen, d->suffix);
  byte_copy(d->query + 69 + m + suffixlen, 4, DNS_T_TXT DNS_C_IN);
}

static int uncurve(const struct dns_transmit *d, char *buf, unsigned int *lenp)
{
  const char *key;
  char nonce[24];
  unsigned int len;
  char out[16];
  unsigned int pos;
  uint16 datalen;
  unsigned int i;
  unsigned int j;
  char ch;
  unsigned int txtlen;
  unsigned int namelen;

  if (!d->keys) return 0;

  key = d->keys + 32 * d->curserver;
  len = *lenp;

  if (!d->suffix) {
    if (len < 48) return 1;
    if (byte_diff(buf, 8, "R6fnvWJ8")) return 1;
    if (byte_diff(buf + 8, 12, d->nonce)) return 1;
    byte_copy(nonce, 24, buf + 8);
    byte_zero(buf + 16, 16);
    if (crypto_box_open_afternm((unsigned char *) buf + 16,
                                (const unsigned char *) buf + 16,
                                len - 16,
                                (const unsigned char *) nonce,
                                (const unsigned char *) key)) {
      curvefail++;
      return 1;
    }
    byte_copyr(buf, len - 48, buf + 48);
    *lenp = len - 48;
    curveok++;
    return 0;
  }

  /* XXX: be more leniant? */
  pos = dns_packet_copy(buf, len, 0, out, 12); if (!pos) return 1;
  if (byte_diff(out, 2, d->query + 2)) return 1;
  if (byte_diff(out + 2, 10, "\204\0\0\1\0\1\0\0\0\0")) return 1;

  /* query name might be >255 bytes, so can't use dns_packet_getname */
  namelen = dns_domain_length(d->query + 14);
  if (namelen > len - pos) return 1;
  if (case_diffb(buf + pos, namelen, d->query + 14)) return 1;
  pos += namelen;

  pos = dns_packet_copy(buf, len, pos, out, 16); if (!pos) return 1;
  if (byte_diff(out, 14, "\0\20\0\1\300\14\0\20\0\1\0\0\0\0")) return 1;
  uint16_unpack_big(out + 14,&datalen);
  if (datalen > len - pos) return 1;

  j = 4;
  txtlen = 0;
  for (i = 0; i < datalen; ++i) {
    ch = buf[pos + i];
    if (!txtlen)
      txtlen = (unsigned char) ch;
    else {
      --txtlen;
      buf[j++] = ch;
    }
  }
  if (txtlen) return 1;

  if (j < 32) return 1;
  byte_copy(nonce, 12, d->nonce);
  byte_copy(nonce + 12, 12, buf + 4);
  byte_zero(buf, 16);
  if (crypto_box_open_afternm((unsigned char *) buf,
                              (const unsigned char *) buf,
                              j,
                              (const unsigned char *) nonce,
                              (const unsigned char *) key)) {
    curvefail++;
    return 1;
  }
  byte_copyr(buf, j - 32, buf + 32);
  *lenp = j - 32;
  curveok++;
  return 0;
}

static void deadline_now(struct dns_transmit *d)
{
  handle_deadline(d);
  taia_now(&d->deadline);
  add_deadline(d);
}

void dns_enable_merge(void (*f)(const char *, const char *, const char *))
{
  merge_enable = 1;
  merge_logger = f;
}

static inline const char* get_domain(struct dns_transmit *d)
{
  if (d->keys)
    return d->name;
  else
    return d->query + 14;
}

uint64 merge_seed[4];

static uint64 merge_hash(struct dns_transmit *x)
{
  const char *nameptr;
  unsigned char mseed[32] __attribute__((aligned(16)));

  /* XXX yyy zzz */
  byte_copy(&mseed[0], 16, x->servers + 16 * x->curserver);
  byte_copy(&mseed[16], 2, x->qtype);
  byte_copy(&mseed[16 + 2], sizeof(merge_seed) - 16 - 2, merge_seed);
  nameptr = get_domain(x);
  return siphash(nameptr, dns_domain_length(nameptr), mseed);
}

static int merge_equal(struct dns_transmit *a, struct dns_transmit *b)
{
  const char *ip1;
  const char *ip2;

  if (a == b) return 1;
  if (!a->query || !b->query) return 0;
  if (a->key != b->key) return 0;
  ip2 = b->servers + 16 * b->curserver;
  ip1 = a->servers + 16 * a->curserver;

  return byte_equal(ip1, 16, ip2) &&
         byte_equal(a->qtype, 2, b->qtype) &&
         dns_domain_equal(get_domain(a), get_domain(b));
}

struct list_head merges_active_table[MERGE_HASH_SIZE];
struct list_head merges_inactive;
struct list_head slaves_inactive;

static void set_merge_inactive(struct dns_transmit *d)
{
  list_move(&d->lmerge, &merges_inactive);
}

static void set_slave_active(struct dns_transmit *from, struct dns_transmit *to)
{
  list_move(&from->lslave, &to->lslaves);
}

static void set_slave_inactive(struct dns_transmit *d)
{
  list_move(&d->lslave, &slaves_inactive);
}

static void register_inprogress(struct dns_transmit *d)
{
  if (!merge_enable) return;

  list_move(&d->lmerge, &merges_active_table[d->key % MERGE_HASH_SIZE]);
}

static void unregister_inprogress(struct dns_transmit *d)
{
  set_merge_inactive(d);
  set_slave_inactive(d);
}

void finish_slave(struct dns_transmit *d)
{
  unregister_inprogress(d);
  handle_deadline(d);
}

static int try_merge(struct dns_transmit *d)
{
  struct dns_transmit *entry, *entrytmp;
  unsigned int currdepth = 0;

  if (!merge_enable) return 0;

  d->key = merge_hash(d);
  list_for_each_entry_safe(entry, entrytmp, &merges_active_table[d->key % MERGE_HASH_SIZE], lmerge) {
    if (merge_equal(entry, d)) {
      d->master = entry;
      set_slave_active(d, entry);
      if (currdepth < ARRAY_SIZE(depths)) depths[currdepth]++;
      return 1;
    }
    currdepth++;
  }
  return 0;
}

int kdpfd = -1;

static uint64 ctr;

static int serverwantstcp(const char *buf,unsigned int len)
{
  char out[12];

  if (!dns_packet_copy(buf,len,0,out,12)) return 1;
  if (out[2] & 2) return 1;
  return 0;
}

static int serverfailed(const char *buf,unsigned int len)
{
  char out[12];
  unsigned int rcode;

  if (!dns_packet_copy(buf,len,0,out,12)) return 1;
  rcode = out[3];
  rcode &= 15;
  if (rcode && (rcode != 3)) { errno = error_again; return 1; }
  return 0;
}

static int irrelevant(const struct dns_transmit *d,const char *buf,unsigned int len)
{
  char out[12];
  char *dn;
  unsigned int pos;

  pos = dns_packet_copy(buf, len, 0, out, 12); if (!pos) return 1;
  if (byte_diff(out, 2, d->nonce + 8)) return 1;

  if (out[4] != 0) return 1;
  if (out[5] != 1) return 1;

  dn = 0;
  pos = dns_packet_getname(buf, len, pos, &dn); if (!pos) return 1;
  if (!dns_domain_equal(dn, d->name)) { alloc_free(dn); return 1; }
  alloc_free(dn);

  pos = dns_packet_copy(buf,len,pos,out,4); if (!pos) return 1;
  if (byte_diff(out,2,d->qtype)) return 1;
  if (byte_diff(out + 2,2,DNS_C_IN)) return 1;

  return 0;
}

static void packetfree(struct dns_transmit *d)
{
  if (!d->packet) return;
  alloc_free(d->packet);
  d->packet = 0;
}

void mergefree(struct dns_transmit *d)
{
  struct dns_transmit *entry, *entrytmp;

  if (!merge_enable) return;

  /* unregister us from our master */
  if (d->master) {
    //set_slave_inactive(d->master);
    d->master = NULL;
  }

  unregister_inprogress(d);

  /* and unregister all of our slaves from us */
  list_for_each_entry_safe(entry, entrytmp, &d->lslaves, lslave) {
    entry->master = NULL;
  }
  handle_deadline(d);
}

static void queryfree(struct dns_transmit *d)
{
  mergefree(d);
  if (!d->query) return;
  alloc_free(d->query);
  d->query = 0;
  d->name = 0;
  d->keys = 0;
  d->pubkey = 0;
  d->suffix = 0;
  byte_zero(d->nonce, sizeof(d->nonce));
}

static void socketfree(struct dns_transmit *d)
{
  int sret;
  socklen_t slen;
  int ret;

  if (!d->s1) return;
  slen = sizeof(sret);
  ret = getsockopt(d->s1 - 1, SOL_SOCKET, SO_ERROR, &sret, &slen);
  close(d->s1 - 1);
  if (ret == 0) errno = sret;
  ut_table[d->s1 - 1].ut = NULL;
  ut_table[d->s1 - 1].client = INACTIVE;
  d->s1 = 0;
  handle_deadline(d);
}

static void socketfree2(struct dns_transmit *d)
{
  int saved_errno;

  if (!d->s1) return;
  saved_errno = errno;
  close(d->s1 - 1);
  errno = saved_errno;
  ut_table[d->s1 - 1].ut = NULL;
  ut_table[d->s1 - 1].client = INACTIVE;
  d->s1 = 0;
  handle_deadline(d);
}

void dns_transmit_free(struct dns_transmit *d)
{
  queryfree(d);
  socketfree(d);
  packetfree(d);
}

static int randombind(struct dns_transmit *d)
{
  int j;

  for (j = 0;j < 10;++j)
    if (socket_bind6(d->s1 - 1,d->localip,1025 + dns_random()%64510,d->scope_id) == 0)
      return 0;
  if (socket_bind6(d->s1 - 1,d->localip,0,d->scope_id) == 0)
    return 0;
  return -1;
}

const int timeouts[4] = { 1, 3, 6, 10 };

static int thisudp(struct dns_transmit *d)
{
  const char *ip;
  struct epoll_event ev = {0};

  socketfree(d);
  mergefree(d);

  while (d->udploop < 4) {
    for (;d->curserver < QUERY_MAXNS;++d->curserver) {
      ip = d->servers + 16 * d->curserver;
      if (!is_v6any(ip)) {
        prepquery(d);

        if (try_merge(d)) {
          merges++;
          if (merge_logger)
            merge_logger(ip, d->qtype, get_domain(d));
          return 0;
        }

        d->s1 = 1 + socket_udp6();
        if (!d->s1) { dns_transmit_free(d); return -1; }
        if (randombind(d) == -1) { dns_transmit_free(d); return -1; }
        ev.data.fd = d->s1 - 1;
        ev.events = EPOLLIN;
        if (epoll_ctl(kdpfd, EPOLL_CTL_ADD, d->s1 - 1, &ev) == -1) return -1;

        if (socket_connect6(d->s1 - 1,ip,53,d->scope_id) == 0) {
          if (send(d->s1 - 1,d->query + 2,d->querylen - 2,0) == d->querylen - 2) {
            struct taia now;

            handle_deadline(d);
            taia_now(&now);
            taia_uint(&d->deadline,timeouts[d->udploop]);
            taia_add(&d->deadline,&d->deadline,&now);
            add_deadline(d);
            d->tcpstate = 0;
            register_inprogress(d);
            return 0;
          }
        }

        socketfree2(d);
      }
    }

    ++d->udploop;
    d->curserver = 0;
  }

  handle_deadline(d);
  dns_transmit_free(d); return -1;
}

static int firstudp(struct dns_transmit *d)
{
  d->curserver = 0;
  return thisudp(d);
}

static int nextudp(struct dns_transmit *d)
{
  ++d->curserver;
  return thisudp(d);
}

static int thistcp(struct dns_transmit *d)
{
  struct taia now;
  const char *ip;
  struct epoll_event ev;

  socketfree(d);
  packetfree(d);

  for (;d->curserver < QUERY_MAXNS;++d->curserver) {
    ip = d->servers + 16 * d->curserver;
    if (!is_v6any(ip)) {
      prepquery(d);

      d->s1 = 1 + socket_tcp6();
      if (!d->s1) { dns_transmit_free(d); return -1; }
      if (randombind(d) == -1) { dns_transmit_free(d); return -1; }

      ev.data.fd = d->s1 - 1;
      ev.events = EPOLLIN;
      if (epoll_ctl(kdpfd, EPOLL_CTL_ADD, d->s1 - 1, &ev) == -1) return -1;
      if (socket_connect6(d->s1 - 1,ip,53,d->scope_id) == 0) {
        handle_deadline(d);
        taia_now(&now);
        taia_uint(&d->deadline, TCPTIMEOUT);
        taia_add(&d->deadline, &d->deadline, &now);
        add_deadline(d);
        d->tcpstate = 2;
        d->pos = 0;
        ev.events = EPOLLOUT;
        if (epoll_ctl(kdpfd, EPOLL_CTL_MOD, d->s1 - 1, &ev) == -1) return -1;
        return 0;
      }
      if ((errno == error_inprogress) || (errno == error_wouldblock)) {
        handle_deadline(d);
        taia_now(&now);
        taia_uint(&d->deadline, TCPTIMEOUT);
        taia_add(&d->deadline, &d->deadline, &now);
        add_deadline(d);
        d->tcpstate = 1;
        return 0;
        ev.events = EPOLLOUT;
        if (epoll_ctl(kdpfd, EPOLL_CTL_MOD, d->s1 - 1, &ev) == -1) return -1;
      }

      socketfree2(d);
    }
  }

  handle_deadline(d);
  dns_transmit_free(d); return -1;
}

static int firsttcp(struct dns_transmit *d)
{
  d->curserver = 0;
  return thistcp(d);
}

static int nexttcp(struct dns_transmit *d)
{
  ++d->curserver;
  return thistcp(d);
}

int dns_transmit_start(struct dns_transmit *d,const char servers[16*QUERY_MAXNS],
                       int flagrecursive,const char *q,const char qtype[2],
                       const char localip[16])
{
  return dns_transmit_start2(d,servers,flagrecursive,q,qtype,localip,0,0);
}

int dns_transmit_start2(struct dns_transmit *d,const char servers[16*QUERY_MAXNS],
                        int flagrecursive,const char *q,const char qtype[2],
                        const char localip[16],const char keys[32*QUERY_MAXNS],
                        const char pubkey[32])
{
  unsigned int len;

  dns_transmit_free(d);
  errno = error_io;

  len = dns_domain_length(q);
  if (!keys)
    d->querylen = len + 18;
  else
    d->querylen = len + 86;

  d->query = alloc(d->querylen);
  if (!d->query) return -1;

  d->name = q;
  byte_copy(d->qtype, 2, qtype);
  d->servers = servers;
  byte_copy(d->localip,16,localip);
  d->fl.flagrecursive = flagrecursive;
  d->keys = keys;
  d->pubkey = pubkey;
  d->suffix = 0;

  if (!d->keys) {
    uint16_pack_big(d->query, len + 16);
    makebasequery(d, d->query + 2);
    d->name = d->query + 14; /* keeps dns_transmit_start backwards compatible */
  }

  d->udploop = flagrecursive ? 1 : 0;
  d->fl.donedead = 0;

  d->ctr = ctr++;
  if (len + 16 > 512) return firsttcp(d);
  return firstudp(d);
}

void dns_transmit_io(struct dns_transmit *d,iopause_fd *x)
{
  struct epoll_event ev = {
    .data.fd = d->s1 - 1
  };
  x->fd = d->s1 - 1;

  switch(d->tcpstate) {
    case 0:
      if (d->master) return;
      if (d->packet) { deadline_now(d); return; }
      /* otherwise, fall through */
    case 3: case 4: case 5:
      ev.events = EPOLLIN;
      epoll_ctl(kdpfd, EPOLL_CTL_MOD, x->fd, &ev);
      break;
    case 1: case 2:
      ev.events = EPOLLOUT;
      epoll_ctl(kdpfd, EPOLL_CTL_MOD, x->fd, &ev);
      break;
  }
}

int dns_transmit_get(struct dns_transmit *d,const iopause_fd *x,
                     const struct taia *when)
{
  char udpbuf[4097];
  unsigned char ch;
  int r;
  int fd;
  struct dns_transmit *entry, *entrytmp;
  unsigned int len;

  errno = error_io;
  fd = d->s1 - 1;

  if (d->tcpstate == 0 && d->master) return 0;
  if (d->tcpstate == 0 && d->packet) return 1;

  if (!(x->revents & (EPOLLIN | EPOLLOUT | EPOLLPRI))) {
    if (x->revents & (EPOLLERR | EPOLLHUP)) {
      /* get rid of errored sockets.
         this has to be done before taia_less or epoll_wait
         keeps returning this fd till deadline is reached. */
      errno = error_timeout;
      if (d->tcpstate == 0) return nextudp(d);
      return nexttcp(d);
    }
    if (taia_less(when,&d->deadline)) return 0;
  }

  if (d->tcpstate == 0) {
/*
have attempted to send UDP query to each server udploop times
have sent query to curserver on UDP socket s
*/
    r = recv(fd,udpbuf,sizeof udpbuf,0);
    if (r <= 0) {
      if (errno == error_connrefused) if (d->udploop == 2) return 0;
      return nextudp(d);
    }
    len = r;
    if (uncurve(d, udpbuf, &len)) return 0;
    if (irrelevant(d,udpbuf,len)) return 0;
    if ((len + 1 > sizeof udpbuf) || serverwantstcp(udpbuf,len)) return firsttcp(d);
    if (serverfailed(udpbuf,len)) {
      if (d->udploop == 2) return 0;
      return nextudp(d);
    }
    socketfree(d);

    d->packetlen = len;
    d->packet = alloc(d->packetlen);
    if (!d->packet) { dns_transmit_free(d); return -1; }
    byte_copy(d->packet,d->packetlen,udpbuf);

    list_for_each_entry_safe(entry, entrytmp, &d->lslaves, lslave) {
      entry->packetlen = d->packetlen;
      entry->packet = alloc(d->packetlen);
      if (!entry->packet) { dns_transmit_free(entry); continue; }
      byte_copy(entry->packet, d->packetlen, udpbuf);
    }

    queryfree(d);
    return 1;
  }

  if (d->tcpstate == 1) {
/*
have sent connection attempt to curserver on TCP socket s
pos not defined
*/
    if (!socket_connected(fd)) return nexttcp(d);
    d->pos = 0;
    d->tcpstate = 2;
    return 0;
  }

  if (d->tcpstate == 2) {
/*
have connection to curserver on TCP socket s
have sent pos bytes of query
*/
    r = write(fd,d->query + d->pos,d->querylen - d->pos);
    if (r <= 0) return nexttcp(d);
    d->pos += r;
    if (d->pos == d->querylen) {
      struct taia now;

      delete_deadline(d);
      taia_now(&now);
      taia_uint(&d->deadline, TCPTIMEOUT);
      taia_add(&d->deadline, &d->deadline, &now);
      add_deadline(d);
      d->tcpstate = 3;
    }
    return 0;
  }

  if (d->tcpstate == 3) {
/*
have sent entire query to curserver on TCP socket s
pos not defined
*/
    errno = 0;
    r = read(fd,&ch,1);
    if (r <= 0) return nexttcp(d);
    d->packetlen = ch;
    d->tcpstate = 4;
    return 0;
  }

  if (d->tcpstate == 4) {
/*
have sent entire query to curserver on TCP socket s
pos not defined
have received one byte of packet length into packetlen
*/
    r = read(fd,&ch,1);
    if (r <= 0) return nexttcp(d);
    d->packetlen <<= 8;
    d->packetlen += ch;
    d->tcpstate = 5;
    d->pos = 0;
    d->packet = alloc(d->packetlen);
    if (!d->packet) { dns_transmit_free(d); return -1; }
    return 0;
  }

  if (d->tcpstate == 5) {
/*
have sent entire query to curserver on TCP socket s
have received entire packet length into packetlen
packet is allocated
have received pos bytes of packet
*/
    r = read(fd,d->packet + d->pos,d->packetlen - d->pos);
    if (r <= 0) return nexttcp(d);
    d->pos += r;
    if (d->pos < d->packetlen) return 0;

    socketfree(d);
    if (uncurve(d,d->packet,&d->packetlen)) return nexttcp(d);
    if (irrelevant(d,d->packet,d->packetlen)) return nexttcp(d);
    if (serverwantstcp(d->packet,d->packetlen)) return nexttcp(d);
    if (serverfailed(d->packet,d->packetlen)) return nexttcp(d);

    queryfree(d);
    return 1;
  }

  return 0;
}
