#ifndef QLOG_H
#define QLOG_H

#include <signal.h>

#include "helper.h"

extern void qlog(const char *,uint16,const char *,const char *,const char *,const char *);
extern volatile sig_atomic_t bequiet;

#endif
