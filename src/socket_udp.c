#include "socket.h"

int socket_udp(void)
{
  int s;

  s = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0);
  if (s == -1) return -1;
  return s;
}
