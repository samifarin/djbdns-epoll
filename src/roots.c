#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>

#include "open.h"
#include "error.h"
#include "str.h"
#include "byte.h"
#include "error.h"
#include "ip4.h"
#include "ip6.h"
#include "dns.h"
#include "openreadclose.h"
#include "roots.h"
#include "siphash.h"
#include "query.h"

#define ROOTS_HTABLE_SZ (1024)
static stralloc data_htable[ROOTS_HTABLE_SZ];
static uint64 root_seed[4];

static stralloc data;

/* If you have for example 111 config files at /service/dnscache/root/servers ,
   roots_find can take almost 50% of dnscache's total CPU time, if not using
   hash table.  With hash table (size 256) it takes around 5%. */
static int roots_find(char *q, stralloc **qsret)
{
  int i;
  uint32 j;
  unsigned int len2;
  uint32 datahash;
  stralloc *datap;

  i = 0;
  len2 = dns_domain_length(q);
  datahash = siphash(q, len2, root_seed);

  datap = &data_htable[datahash % ROOTS_HTABLE_SZ];
  *qsret = datap;
  /* datap->s contains hash (uint32), length (uint32), servers (16*QUERY_MAXNS bytes) */

  while (i < datap->len) {
    byte_copy(&j, 4, &datap->s[i+4]);
    if (byte_equal(&datap->s[i], 4, (char*)&datahash) &&
        dns_domain_equal_len(&datap->s[i + 8], q, j, len2)) return i + 8 + j;
    i += 8 + j + 16*QUERY_MAXNS;
  }
  return -1;
}

static int roots_search(char *q, stralloc **qsret)

{
  int r;
  stralloc *qs;

  for (;;) {
    r = roots_find(q, &qs);
    if (r >= 0) { *qsret = qs; return r; }
    if (!*q) return -1; /* user misconfiguration */
    q += *q;
    q += 1;
  }
}

int roots(char servers[16*QUERY_MAXNS],char *q)
{
  int r;
  stralloc *qs;

  r = roots_find(q, &qs);
  if (r == -1) return 0;
  byte_copy(servers, 16*QUERY_MAXNS, &qs->s[r]);
  return 1;
}

int roots_same(char *q,char *q2)
{
  stralloc *qs1 = NULL, *qs2 = NULL;
  int qr1, qr2;

  qr1 = roots_search(q, &qs1);
  qr2 = roots_search(q2, &qs2);

  return ((qr1 == qr2) && (qs1 == qs2));
}

static int init2(DIR *dir)
{
  struct dirent *d;
  const char *fqdn;
  static char *q;
  static stralloc text;
  char servers[16*QUERY_MAXNS];
  int serverslen;
  int i;
  int j;
  uint32 dlen;
  uint32 datahash;
  stralloc *p;

  for (;;) {
    errno = 0;
    d = readdir(dir);
    if (!d) {
      if (errno) return 0;
      return 1;
    }

    if (d->d_name[0] != '.') {
      if (openreadclose(d->d_name,&text,512) != 1) return 0;
      if (!stralloc_append(&text,"\n")) return 0;

      fqdn = d->d_name;
      if (str_equal(fqdn,"@")) fqdn = ".";
      if (!dns_domain_fromdot(&q,fqdn,str_len(fqdn))) return 0;

      serverslen = 0;
      j = 0;
      for (i = 0;i < text.len;++i)
        if (text.s[i] == '\n') {
          if (serverslen <= (16*(QUERY_MAXNS - 1)))
            if (ip6_scan(text.s + j,servers + serverslen))
              serverslen += 16;
          j = i + 1;
        }
      byte_zero(servers + serverslen,16*QUERY_MAXNS - serverslen);

      dlen = dns_domain_length(q);
      datahash = siphash(q, dlen, root_seed);
      p = &data_htable[datahash % ROOTS_HTABLE_SZ];

      if (!stralloc_catb(p, (char*)&datahash, 4)) return 0;
      if (!stralloc_catb(p, (char*)&dlen, 4)) return 0;
      if (!stralloc_catb(p, q, dlen)) return 0;
      if (!stralloc_catb(p, servers, 16*QUERY_MAXNS)) return 0;
    }
  }
}

static int init1(void)
{
  DIR *dir;
  int r;

  if (chdir("servers") == -1) return 0;
  dir = opendir(".");
  if (!dir) return 0;
  r = init2(dir);
  closedir(dir);
  return r;
}

int roots_init(void)
{
  int fddir;
  int r;

  if (!stralloc_copys(&data,"")) return 0;
  for (r = 0; r < ROOTS_HTABLE_SZ; r++) {
    if (!stralloc_copys(&data_htable[r], "")) return 0;
  }
  charcrandom_buf(root_seed, sizeof(root_seed));

  fddir = open_read(".");
  if (fddir == -1) return 0;
  r = init1();
  if (fchdir(fddir) == -1) r = 0;
  close(fddir);
  return r;
}
