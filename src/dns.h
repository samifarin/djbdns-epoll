#ifndef DNS_H
#define DNS_H

#include <signal.h>

#include "stralloc.h"
#include "iopause.h"
#include "taia.h"
#include "error.h"
#include "case.h"
#include "byte.h"
#include "alloc.h"
#include "list.h"
#include "rbtree.h"
#include "helper.h"
#include "charcrandom.h"
#include "uint16_inline.h"
#include "uint32_inline.h"

#define MAXUDP (8192U)
#define MAXTCP (128U)

#define MERGE_HASH_SIZE (MAXUDP*2)

#define DNS_C_IN "\0\1"
#define DNS_C_ANY "\0\377"

#define DNS_T_A "\0\1"
#define DNS_T_NS "\0\2"
#define DNS_T_CNAME "\0\5"
#define DNS_T_SOA "\0\6"
#define DNS_T_PTR "\0\14"
#define DNS_T_HINFO "\0\15"
#define DNS_T_MX "\0\17"
#define DNS_T_TXT "\0\20"
#define DNS_T_RP "\0\21"
#define DNS_T_SIG "\0\30"
#define DNS_T_KEY "\0\31"
#define DNS_T_AAAA "\0\34"
#define DNS_T_OPT "\0\51"
#define DNS_T_DS "\0\53"
#define DNS_T_RRSIG "\0\56"
#define DNS_T_DNSKEY "\0\60"
#define DNS_T_NSEC3 "\0\62"
#define DNS_T_NSEC3PARAM "\0\63"
#define DNS_T_AXFR "\0\374"
#define DNS_T_ANY "\0\377"
/* Pseudo-RRs for DNSSEC */
#define DNS_T_HASHREF "\377\1"
#define DNS_T_HASHLIST "\377\2"

#define TCPTIMEOUT (10)

enum e_client {
  INACTIVE,
  UDP_CLIENT,
  TCP_CLIENT,
};

struct dns_transmit {
  struct rb_node rb_timeout;
  uint64 ctr;
  char *query; /* 0, or dynamically allocated */
  char *packet; /* 0, or dynamically allocated */
  const char *servers;
  union {
    struct udpclient *u;
    struct tcpclient *t;
  } ctx;
  unsigned int querylen;
  unsigned int packetlen;
  int s1; /* 0, or 1 + an open file descriptor */
  unsigned int udploop;
  unsigned int curserver;
  struct taia deadline;
  unsigned int pos;
  /* dnscurve start */
  const char *name;
  const char *keys;
  const char *pubkey;
  const char *suffix;
  char nonce[12];
  /* dnscurve end */
  struct dns_transmit *master;
  struct list_head lmerge;
  struct list_head lslave;
  struct list_head lslaves;
  uint64 key;
  char localip[16];
  unsigned int scope_id;
  char qtype[2];
  char tcpstate;
  enum e_client client;
  struct {
    unsigned int donedead:1; /* have added timeout into RB tree */
    unsigned int flagrecursive:1;
  } fl;
};

extern struct list_head merges_active_table[MERGE_HASH_SIZE];
extern struct list_head merges_inactive;
extern struct list_head slaves_inactive;
extern volatile sig_atomic_t flagstats;
extern uint64 merges;
extern int merge_enable;
extern uint64 depths[5];

extern void finish_slave(struct dns_transmit *d);
extern void dns_enable_merge(void (*logger)(const char *, const char *,
                             const char *));

#include "query.h"

struct udpclient {
  uint64 active; /* query number, if active; otherwise 0 */
  struct list_head list;
  struct query q;
  struct taia start;
  iopause_fd io;
  char ip[16];
  uint32 scope_id;
  uint16 port;
  char id[2];
};

struct tcpclient {
  uint64 active; /* query number or 1, if active; otherwise 0 */
  struct list_head list;
  struct query q;
  struct taia start;
  struct taia timeout;
  iopause_fd io;
  int tcp; /* open TCP socket, if active */
  int state;
  char *buf; /* 0, or dynamically allocated of length len */
  unsigned int len;
  unsigned int pos;
  char ip[16]; /* send response to this address */
  uint32 scope_id;
  uint16 port; /* send response to this port */
  char id[2];
};

extern int kdpfd;
extern const int timeouts[4];

struct ut_table_t {
  void* ut;     /* contains struct udpclient* or tcpclient* */
  enum e_client client;
};
extern struct ut_table_t ut_table[];

extern void delete_deadline(struct dns_transmit*);
extern void add_deadline(struct dns_transmit*);
static inline void handle_deadline(struct dns_transmit *d)
{
  if (d->fl.donedead) {
    delete_deadline(d);
  }
}

extern void dns_random_init();
extern int dns_epoll_init(int);
extern uint16 dns_random(void);

extern void dns_sortip6(char *,unsigned int);

/* randomize server IPv6 addresses and keys;
   keys are shuffled to the same server position ;) */
static inline void dns_sortip2(char *s,char *t)
{
  uint32 i;
  char tmp[32];
  uint32 n = QUERY_MAXNS;

  while (n > 1) {
    i = charcrandom_u32() % n;
    --n;
    byte_copy(tmp, 16, s + (i * 16));
    byte_copy(s + (i * 16), 16, s + (n * 16));
    byte_copy(s + (n * 16), 16, tmp);
    byte_copy(tmp, 32, t + (i * 32));
    byte_copy(t + (i * 32), 32, t + (n * 32));
    byte_copy(t + (n * 32), 32, tmp);
  }
}

static inline void dns_sortip(char *s,unsigned int n)
{
  int i;
  unsigned int rnd;
  char tmp[4];

  n >>= 2;
  if (n <= 1) return;
  for (i = n - 1; i >= 0; i--) {
    rnd = dns_random() % n;
    byte_copy(tmp,4,s + (i << 2));
    byte_copy(s + (i << 2),4,s + (rnd << 2));
    byte_copy(s + (rnd << 2),4,tmp);
  }
}

extern void dns_domain_free(char **);
extern int dns_domain_copy(char **,const char *);
//extern unsigned int dns_domain_length(const char *);
//extern int dns_domain_equal(const char *,const char *);
extern int dns_domain_suffix(const char *,const char *);
extern unsigned int dns_domain_suffixpos(const char *,const char *);
extern int dns_domain_fromdot(char **,const char *,unsigned int);
extern int dns_domain_todot_cat(stralloc *,const char *);

//extern unsigned int dns_packet_copy(const char *,unsigned int,unsigned int,char *,unsigned int);
extern unsigned int dns_packet_getname(const char *,unsigned int,unsigned int,char **);
extern unsigned int dns_packet_skipname(const char *,unsigned int,unsigned int);

extern int dns_transmit_start(struct dns_transmit *,const char *,int,const char *,const char *,const char *);
extern int dns_transmit_start2(struct dns_transmit *,const char *,int,const char *,const char *,const char *,const char *,const char *);
extern void dns_transmit_free(struct dns_transmit *);
extern void dns_transmit_io(struct dns_transmit *,iopause_fd *);
extern int dns_transmit_get(struct dns_transmit *,const iopause_fd *,const struct taia *);

extern int dns_resolvconfip(char *);
extern int dns_resolve(const char *,const char *);
extern struct dns_transmit dns_resolve_tx;

extern int dns_ip4_packet(stralloc *,const char *,unsigned int);
extern int dns_ip4(stralloc *,const stralloc *);
extern int dns_ip6_packet(stralloc *,char *,unsigned int);
extern int dns_ip6(stralloc *,stralloc *);
extern int dns_name_packet(stralloc *,const char *,unsigned int);
extern void dns_name4_domain(char *,const char *);
#define DNS_NAME4_DOMAIN 31
extern int dns_name4(stralloc *,const char *);
extern int dns_name6(stralloc *,const char *);
extern int dns_txt_packet(stralloc *,const char *,unsigned int);
extern int dns_txt(stralloc *,const stralloc *);
extern int dns_mx_packet(stralloc *,const char *,unsigned int);
extern int dns_mx(stralloc *,const stralloc *);

extern int dns_resolvconfrewrite(stralloc *);
extern int dns_ip4_qualify_rules(stralloc *,stralloc *,const stralloc *,const stralloc *);
extern int dns_ip4_qualify(stralloc *,stralloc *,const stralloc *);
extern int dns_ip6_qualify_rules(stralloc *,stralloc *,const stralloc *,const stralloc *);
extern int dns_ip6_qualify(stralloc *,stralloc *,const stralloc *);

#define DNS_IP6_INT 0
#define DNS_IP6_ARPA 1

extern int dns_name6_domain(char *,const char *,int);
#define DNS_NAME6_DOMAIN (4*16+11)

static inline unsigned int dns_packet_copy(const char *buf,unsigned int len,unsigned int pos,char *out,unsigned int outlen)
{
  while (outlen) {
    if (pos >= len) { errno = error_proto; return 0; }
    *out = buf[pos++];
    ++out; --outlen;
  }
  return pos;
}

static inline int dns_domain_copy_len(char **out, const char *in, unsigned int len)
{
  char *x;

  x = alloc(len);
  if (!x) return 0;
  byte_copy(x,len,in);
  if (*out) alloc_free(*out);
  *out = x;
  return 1;
}

static inline unsigned int dns_domain_length(const char *dn)
{
  const char *x;
  unsigned char c;

  x = dn;
  while ((c = *x++))
    x += (unsigned int) c;
  return x - dn;
}

static inline int dns_domain_equal(const char *dn1,const char *dn2)
{
  unsigned int len;

  len = dns_domain_length(dn1);
  if (len != dns_domain_length(dn2)) return 0;

  if (case_diffb(dn1,len,dn2)) return 0; /* safe since 63 < 'A' */
  return 1;
}

static inline int dns_domain_equal_len(const char *dn1, const char *dn2, const unsigned int len1, const unsigned int len2)
{
  if (len1 != len2) return 0;

  if (case_diffb(dn1,len1,dn2)) return 0; /* safe since 63 < 'A' */
  return 1;
}

extern void dns_sortip2(char *,char *);

#endif
