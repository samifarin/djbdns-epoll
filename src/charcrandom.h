#pragma once

#include <stddef.h>
#include <stdint.h>
#include <string.h>

extern uint16_t charcrandom_u16(void);
extern uint32_t charcrandom_u32(void);
extern uint64_t charcrandom_u64(void);
extern void charcrandom_addrandom(uint8_t *dat, size_t datlen);
extern void charcrandom_buf(void *buf, size_t n);
extern uint32_t charcrandom_uniform32(uint32_t upper_bound);
extern uint64_t charcrandom_uniform64(uint64_t upper_bound);
static void* (* const volatile _secure_memset)(void*, int, size_t) = memset;
static inline void* secure_memset(void* p, size_t n)
{
    return _secure_memset(p, 0, n);
}
