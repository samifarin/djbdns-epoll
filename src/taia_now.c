#include <time.h>
#include <sys/types.h>
#include "taia.h"

void taia_now(struct taia *t)
{
  struct timespec now;

  clock_gettime(CLOCK_REALTIME, &now);
  tai_unix(&t->sec,now.tv_sec);
  t->nano = now.tv_nsec;
  t->atto = 0;
}

void taia_now_put(struct taia *t, struct timespec *tv)
{
  tai_unix(&t->sec, tv->tv_sec);
  t->nano = tv->tv_nsec;
  t->atto = 0;
}
