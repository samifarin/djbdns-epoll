#include <inttypes.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/resource.h>
/* for offsetof */
#include <stddef.h>

#include "env.h"
#include "exit.h"
#include "scan.h"
#include "strerr.h"
#include "error.h"
#include "ip4.h"
#include "ip6.h"
#include "socket.h"
#include "dns.h"
#include "taia.h"
#include "byte.h"
#include "roots.h"
#include "fmt.h"
#include "iopause.h"
#include "query.h"
#include "alloc.h"
#include "response.h"
#include "cache.h"
#include "log.h"
#include "droproot.h"
#include "list.h"
#include "rbtree.h"
#include "openreadclose.h"

static void usr1handler(int sign)
{
  if (sign == SIGUSR1)
    bequiet = 0;
}

static void usr2handler(int sign)
{
  if (sign == SIGUSR2)
    bequiet = 1;
}

static void huphandler(int sign)
{
  if (sign == SIGHUP)
    flagstats = 1;
}

static void signal_setup(void)
{
  struct sigaction act1 = {
    .sa_handler = usr1handler,
  };
  struct sigaction act2 = {
    .sa_handler = usr2handler,
  };
  struct sigaction acthup = {
    .sa_handler = huphandler,
  };

  sigaction(SIGUSR1, &act1, NULL);
  sigaction(SIGUSR2, &act2, NULL);
  sigaction(SIGHUP, &acthup, NULL);
}

static long interface;

stralloc ignoreip = {0};
int noipv6;

static int packetquery(char *buf,unsigned int len,char **q,char qtype[2],char qclass[2],char id[2])
{
  unsigned int pos;
  char header[12];

  errno = error_proto;
  pos = dns_packet_copy(buf,len,0,header,12); if (!pos) return 0;
  if (header[2] & 128) return 0; /* must not respond to responses */
  if (!(header[2] & 1)) return 0; /* do not respond to non-recursive queries */
  if (header[2] & 120) return 0;
  if (header[2] & 2) return 0;
  if (byte_diff(header + 4,2,"\0\1")) return 0;

  pos = dns_packet_getname(buf,len,pos,q); if (!pos) return 0;
  pos = dns_packet_copy(buf,len,pos,qtype,2); if (!pos) return 0;
  pos = dns_packet_copy(buf,len,pos,qclass,2); if (!pos) return 0;
  if (byte_diff(qclass,2,DNS_C_IN) && byte_diff(qclass,2,DNS_C_ANY)) return 0;

  byte_copy(id,2,header);
  return 1;
}


static char myipoutgoing[16];
static char myipincoming[16];
static char dnsbuf[4096];
uint64 numqueries;
uint64 tcpnumqueries = (uint64)(1ULL<<63);
uint64 merges;
extern uint64 merge_seed[4];

volatile sig_atomic_t flagstats;
int kdpfd;
struct epoll_event events[MAXUDP + MAXTCP];

#define MAXSOA 20
int soaactive = 0;

static int udp53;

static struct udpclient u[MAXUDP];
uint64 uactive;
uint64 udpdrop;

static struct list_head udp_active;
static struct list_head udp_inactive;

static int tcp53;

static struct tcpclient t[MAXTCP];
uint64 tactive;
uint64 tcpdrop;

static struct list_head tcp_active;
static struct list_head tcp_inactive;

static struct rb_root query_deadline;
static struct rb_node *rb_leftmost;

/* this table indexed by fd returned by epoll_wait */
struct ut_table_t ut_table[MAXUDP + MAXTCP +
                           10 /* usually just 3 would be enough*/ ];

void delete_deadline(struct dns_transmit *d)
{
  if (rb_leftmost == &d->rb_timeout)
    rb_leftmost = rb_next(&d->rb_timeout);

  rb_erase(&d->rb_timeout, &query_deadline);
  d->fl.donedead = 0;
}

static long int deadline_cmp(struct taia *t1, struct taia *t2)
{
  if (t1->sec.x == t2->sec.x) {
    if (unlikely(t1->nano == t2->nano)) {
      if (unlikely(t1->atto == t2->atto)) {
        return 0;
      } else {
        return (t1->atto - t2->atto);
      }
    } else {
      return (t1->nano - t2->nano);
    }
  } else {
    return (t1->sec.x - t2->sec.x);
  }
}

static struct dns_transmit *rb_insert_deadline(struct rb_root *root,
                                                      struct dns_transmit *d)
{
  struct rb_node **p = &root->rb_node;
  struct rb_node *parent = NULL;
  struct dns_transmit *dtmp;
  long int ret;
  int leftmost = 1;

  while (*p) {
    parent = *p;
    dtmp = rb_entry(parent, struct dns_transmit, rb_timeout);
    ret = deadline_cmp(&d->deadline, &dtmp->deadline);
    if (ret == 0) ret = ((long int)d->ctr - (long int)dtmp->ctr);
    if (ret < 0) {
      p = &(*p)->rb_left;
    } else if (ret > 0) {
      p = &(*p)->rb_right;
      leftmost = 0;
    } else {
      return d;
    }
  }

  if (leftmost)
    rb_leftmost = &d->rb_timeout;

  rb_link_node(&d->rb_timeout, parent, p);
  rb_insert_color(&d->rb_timeout, root);
  return NULL;
}

void add_deadline(struct dns_transmit *d)
{
  rb_insert_deadline(&query_deadline, d);
  d->fl.donedead = 1;
}

static void u_respond(struct udpclient *x);

static void udp_del(struct udpclient *x)
{
  struct dns_transmit *ptr, *ptrtmp;

  if (merge_enable) {
    list_for_each_entry_safe(ptr, ptrtmp, &x->q.dt.lslaves, lslave) {
      finish_slave(ptr);     
      u_respond(ptr->ctx.u);
    }
  }

  list_move_tail(&x->list, &udp_inactive);
  if (x->active) {
    x->active = 0;
    --uactive;
  }
  handle_deadline(&x->q.dt);
}

static void inline udp_add(struct udpclient *x)
{
  list_move_tail(&x->list, &udp_active);
}

static void u_drop(struct udpclient *x)
{
  if (!x->active) return;
  log_querydrop(&x->active);
  udp_del(x);
  udpdrop++;
}

static void t_free(struct tcpclient *x);
static void t_close(struct tcpclient *x);

static void t_respond(struct tcpclient *x)
{
  struct epoll_event ev;

  if (!x->active) return;
  log_querydone(&x->active,response_len);

  response_id(x->id);
  x->len = response_len + 2;
  t_free(x);
  x->buf = alloc(response_len + 2);
  if (!x->buf) { t_close(x); return ; }
  uint16_pack_big(x->buf,response_len);
  byte_copy(x->buf + 2,response_len,response);
  x->pos = 0;
  x->state = -1;
  ev.events = EPOLLOUT;
  ev.data.fd = x->tcp;
  epoll_ctl(kdpfd, EPOLL_CTL_MOD, x->tcp, &ev);
}

static void u_respond(struct udpclient *x)
{
  if (!x->active) return;
  response_id(x->id);
  if (response_len > 512) response_tc();
  socket_send6(udp53,response,response_len,x->ip,x->port,x->scope_id);
  log_querydone(&x->active,response_len);
  udp_del(x);
}


static void u_new(void)
{
  struct udpclient *x;
  int len;
  static char *q = NULL;
  char qtype[2];
  char qclass[2];
  uint16 port;
  char ip[16];
  char id[2];
  int sno;
  int ret;
  uint32 scope_id;

  while (1) {
    len = socket_recv6(udp53,dnsbuf,sizeof dnsbuf,ip,&port,&scope_id);
    if (len == -1) break;
    if (len >= sizeof dnsbuf) continue;
    if (port < 1024) if (port != 53) continue;
    if (!packetquery(dnsbuf,len,&q,qtype,qclass,id)) return;

    if (list_empty(&udp_inactive)) {
      x = list_entry(udp_active.next, struct udpclient, list);
      errno = error_timeout;
      /* drop oldest active */
      u_drop(x);
    }

    x = list_entry(udp_inactive.next, struct udpclient, list);

    taia_now(&x->start);

    byte_copy(x->ip,16,ip);
    x->port = port;
    x->scope_id = scope_id;
    byte_copy(x->id,2,id);

    x->active = ++numqueries; ++uactive;
    log_query(&x->active,x->ip,x->port,x->id,q,qtype);
    x->q.dt.ctx.u = x;
    x->q.dt.client = UDP_CLIENT;
    ret = query_start(&x->q,q,qtype,qclass,myipoutgoing,interface);
    sno = x->q.dt.s1;
    udp_add(x);
    if (sno) {
      query_io(&x->q, &x->io);
      ut_table[sno-1].ut = x;
      ut_table[sno-1].client = UDP_CLIENT;
    }

    switch (ret) {
      case -1:
        u_drop(x);
        return;
      case 1:
        u_respond(x);
    }
  }
}

static void inline refresh_tcpclient_deadline(struct tcpclient *x)
{
  list_move_tail(&x->list, &tcp_active);
}

/*
state 1: buf 0; normal state at beginning of TCP connection
state 2: buf 0; have read 1 byte of query packet length into len
state 3: buf allocated; have read pos bytes of buf
state 0: buf 0; handling query in q
state -1: buf allocated; have written pos bytes
*/

static void t_free(struct tcpclient *x)
{
  if (!x->buf) return;
  alloc_free(x->buf);
  x->buf = NULL;
}

static void t_timeout(struct tcpclient *x)
{
  struct taia now;

  if (!x->active) return;
  taia_now(&now);
  taia_uint(&x->timeout,TCPTIMEOUT);
  taia_add(&x->timeout,&x->timeout,&now);
}

static void tcp_del(struct tcpclient *x)
{
  struct dns_transmit *ptr, *ptrtmp;

  if (merge_enable) {
    list_for_each_entry_safe(ptr, ptrtmp, &x->q.dt.lslaves, lslave) {
      finish_slave(ptr);
      t_respond(ptr->ctx.t);
    }
  }

  list_move_tail(&x->list, &tcp_inactive);
  if (x->active) {
    x->active = 0;
    --tactive;
  }
  handle_deadline(&x->q.dt);
}

static void inline tcp_add(struct tcpclient* x)
{
  list_move_tail(&x->list, &tcp_active);
}

static void t_close(struct tcpclient *x)
{
  int saved_errno;

  saved_errno = errno;
  if (!x->active) return;
  t_free(x);
  errno = saved_errno;
  log_tcpclose(x->ip,x->port);
  close(x->tcp);
  ut_table[x->tcp].ut = NULL;
  ut_table[x->tcp].client = INACTIVE;
  x->tcp = -1;
  tcp_del(x);
}

static void t_close_silent(struct tcpclient *x)
{
  if (!x->active) return;
  t_free(x);
  close(x->tcp);
  ut_table[x->tcp].ut = NULL;
  ut_table[x->tcp].client = INACTIVE;
  x->tcp = -1;
  tcp_del(x);
}

static void t_drop(struct tcpclient *x)
{
  log_querydrop(&x->active);
  errno = error_pipe;
  t_close(x);
  tcpdrop++;
}

static inline int socket_inqueue(int s)
{
  int val;

  if (ioctl(s, SIOCINQ, &val) == 0) return val;
  return -1;
}

static void t_rw(struct tcpclient *x)
{
  char ch;
  static char *q = NULL;
  char qtype[2];
  char qclass[2];
  int r;
  int ret;
  int sno;
  int inq;

  if (x->state == -1) {
    r = write(x->tcp,x->buf + x->pos,x->len - x->pos);
    if (r <= 0) { t_close(x); return; }
    x->pos += r;
    if (x->pos == x->len) {
      t_close_silent(x);
      x->state = 1; /* could drop connection immediately */
    }
    return;
  }

  errno = 0;
  r = read(x->tcp,&ch,1);
  if (r == 0) { errno = error_pipe; t_close(x); return; }
  if (r < 0) { t_close(x); tcpdrop++; return; }

  if (x->state == 1) {
    x->len = (unsigned char) ch;
    x->len <<= 8;
    x->state = 2;
    return;
  }
  if (x->state == 2) {
    x->len += (unsigned char) ch;
    if (!x->len) { errno = error_proto; t_close(x); return; }
    x->buf = alloc(x->len);
    if (!x->buf) { t_close(x); return; }
    x->pos = 0;
    x->state = 3;
    return;
  }

  if (x->state != 3) t_drop(x); /* impossible */

  x->buf[x->pos++] = ch;
  /* four system calls per one read char sucks. */
  inq = socket_inqueue(x->tcp);
  if (inq == -1) { t_close(x); return; }
  if (inq > (x->len - x->pos)) inq = (x->len - x->pos);
  if (inq > 0) {
    r = read(x->tcp, &x->buf[x->pos], inq);
    if (r <= 0) { if (r == 0) errno = error_pipe; t_close(x); return; }
    x->pos += r;
  }
  if (x->pos < x->len) return;

  if (!packetquery(x->buf,x->len,&q,qtype,qclass,x->id)) { t_close(x); return; }

  x->active = ++numqueries;
  log_query(&x->active,x->ip,x->port,x->id,q,qtype);
  x->q.dt.ctx.t = x;
  x->q.dt.client = TCP_CLIENT;
  ret = query_start(&x->q,q,qtype,qclass,myipoutgoing,interface);
  sno = x->q.dt.s1;
  if (sno) {
    query_io(&x->q, &x->io);
    ut_table[sno-1].ut = x;
    ut_table[sno-1].client = TCP_CLIENT;
  }
  switch(ret) {
    case -1:
      t_drop(x);
      return;
    case 1:
      t_respond(x);
      return;
  }
  t_free(x);
  x->state = 0;
}

static void t_new(void)
{
  struct tcpclient *x;
  struct epoll_event ev;

  if (list_empty(&tcp_inactive)) {
    x = list_entry(tcp_active.next, struct tcpclient, list);
    errno = error_timeout;
    if (x->state == 0)
      t_drop(x);
    else {
      t_close(x);
      tcpdrop++; /* could do this in some other places, too */
    }
  }

  x = list_entry(tcp_inactive.next, struct tcpclient, list);
  taia_now(&x->start);

  x->tcp = socket_accept6(tcp53,x->ip,&x->port,&x->scope_id);
  if (x->tcp == -1) return;
  if (x->port < 1024) if (x->port != 53) { close(x->tcp); return; }
  /* socket_tryreservein ? */
  ev.events = EPOLLIN;
  ev.data.fd = x->tcp;
  if (epoll_ctl(kdpfd, EPOLL_CTL_ADD, x->tcp, &ev) == -1) {
    close(x->tcp);
    return;
  }

  ut_table[x->tcp].ut = x;
  ut_table[x->tcp].client = TCP_CLIENT;
  x->active = ++tcpnumqueries; ++tactive;
  x->state = 1;
  t_timeout(x);
  tcp_add(x);
  refresh_tcpclient_deadline(x);

  log_tcpopen(x->ip,x->port);
}

static void put_next_deadline(struct taia *dead)
{
  struct udpclient *xu;
  struct tcpclient *xt;
  struct rb_node *rbtmp;
  struct dns_transmit *d;

  rbtmp = rb_leftmost;
  if (rbtmp) {
    d = rb_entry(rbtmp, struct dns_transmit, rb_timeout);
    switch (d->client) {
      case UDP_CLIENT:
        xu = d->ctx.u;
        if (taia_less(&xu->q.dt.deadline, dead)) {
          *dead = xu->q.dt.deadline;
        }
        break;

      case TCP_CLIENT:
        xt = d->ctx.t;
        if (taia_less(&xt->q.dt.deadline, dead)) {
          *dead = xt->q.dt.deadline;
        }
        break;

      case INACTIVE:
        break;
    }
  }

  if (!list_empty(&tcp_active)) {
    xt = list_entry(tcp_active.next, struct tcpclient, list);
    if (taia_less(&xt->timeout, dead)) {
      *dead = xt->timeout;
    }
  }
}

/* Returns: 1 if no more old enough udpclients / tcpclients to process */
static int process_timeouts(enum e_client which,
                             struct udpclient *xu,
                             struct tcpclient *xt,
                             struct taia *stamp)
{
  int r;
  int fd;
  int flagout = 0;

  switch(which) {
    case UDP_CLIENT:
      if (xu->active) {
        if (taia_less(&xu->q.dt.deadline, stamp)) {
          xu->io.revents = 0;
          r = query_get(&xu->q,&xu->io, stamp);
          if (r == -1) {
            u_drop(xu);
          } else if (r == 1) {
            u_respond(xu);
          } else {
            fd = xu->q.dt.s1;
            if (fd) {
              /* retrying query with another fd, setup pointers */
              fd--;
              ut_table[fd].ut = xu;
              ut_table[fd].client = UDP_CLIENT;
            }
          }
        } else {
          flagout = 1;
        }
      }
      break;

    case TCP_CLIENT:
      if (xt->active) {
        if (xt->state == 0) {
          xt->io.revents = 0;
          r = query_get(&xt->q, &xt->io, stamp);
          if (r == -1) {
            t_drop(xt);
          } else if (r == 1) {
            t_respond(xt);
          } else {
            fd = xt->q.dt.s1;
            if (fd) {
              /* retrying query with another fd, setup pointers */
              fd--;
              ut_table[fd].ut = xt;
              ut_table[fd].client = TCP_CLIENT;
            }
          }
          if (taia_less(&xt->timeout, stamp))
            t_rw(xt);
        } else {
          if (taia_less(&xt->timeout, stamp)) {
            t_rw(xt);
          } else {
            /* next entries have not reached xt->timeout deadline */
            flagout = 1;
          }
        }
      }
      break;

    case INACTIVE:
      break;
  }

  return flagout;
}

#define FATAL "dnscache: fatal: "

static void doit(void)
{
  struct taia deadline;
  struct taia stamp;
  int r;
  int i;
  int nfds;
  int fd;
  struct epoll_event ev = {0};
  int maxwait;
  struct rb_node *rbtmp;
  struct udpclient *xu;
  struct tcpclient *xt;
  struct dns_transmit *d;
  struct tcpclient *tcpcltmp;
  struct tcpclient *tcpcl;
  enum e_client which;

  kdpfd = epoll_create(MAXUDP+MAXTCP);
  if (kdpfd == -1) {
    strerr_die2sys(111,FATAL,"epoll_create: ");
  }

  ev.data.fd = udp53;
  ev.events = EPOLLIN;
  if (epoll_ctl(kdpfd, EPOLL_CTL_ADD, udp53, &ev) == -1) {
    strerr_die2sys(111,FATAL,"epoll_ctl add udp53: ");
  }
  ev.data.fd = tcp53;
  ev.events = EPOLLIN;
  if (epoll_ctl(kdpfd, EPOLL_CTL_ADD, tcp53, &ev) == -1) {
    strerr_die2sys(111,FATAL,"epoll_ctl add tcp53: ");
  }

  INIT_LIST_HEAD(&tcp_active);
  INIT_LIST_HEAD(&tcp_inactive);
  INIT_LIST_HEAD(&udp_active);
  INIT_LIST_HEAD(&udp_inactive);
  INIT_LIST_HEAD(&merges_inactive);
  INIT_LIST_HEAD(&slaves_inactive);
  for (i = 0; i < MAXUDP; i++) {
    INIT_LIST_HEAD(&u[i].list);
    list_move_tail(&u[i].list, &udp_inactive);
    INIT_LIST_HEAD(&u[i].q.dt.lmerge);
    INIT_LIST_HEAD(&u[i].q.dt.lslave);
    INIT_LIST_HEAD(&u[i].q.dt.lslaves);
    list_move_tail(&u[i].q.dt.lmerge, &merges_inactive);
    list_move_tail(&u[i].q.dt.lslave, &slaves_inactive);
  }
  for (i = 0; i < MAXTCP; i++) {
    INIT_LIST_HEAD(&t[i].list);
    list_move_tail(&t[i].list, &tcp_inactive);
    INIT_LIST_HEAD(&t[i].q.dt.lmerge);
    INIT_LIST_HEAD(&t[i].q.dt.lslave);
    INIT_LIST_HEAD(&t[i].q.dt.lslaves);
    list_move_tail(&t[i].q.dt.lmerge, &merges_inactive);
    list_move_tail(&t[i].q.dt.lslave, &slaves_inactive);
  }
  for (i = 0; i < MERGE_HASH_SIZE; i++)
    INIT_LIST_HEAD(&merges_active_table[i]);

  query_deadline = RB_ROOT;

  /* let's remember to purge every now and then... */
  maxwait = min_t(int, (MAXUDP+MAXTCP), 1000);

  for (;;) {
    taia_now(&stamp);
    taia_uint(&deadline,120);
    taia_add(&deadline,&deadline,&stamp);
    put_next_deadline(&deadline);

    if (flagstats) {
      log_stats();
      log_miscstats();
      flagstats = 0;
    }

    nfds = iopause(kdpfd, events, maxwait, &deadline, &stamp);

    for (i = 0; i < nfds; i++) {
      /* If fd or nfds are out of bounds, report to LKML */
      fd = events[i].data.fd;

      if (fd == udp53) {
        u_new();
      } else if (fd == tcp53) {
        t_new();
      } else {
        if (ut_table[fd].ut == NULL) {
          /* kernel bug */
          close(fd);
          continue;
        }

        which = ut_table[fd].client;
        switch (which) {
          case UDP_CLIENT:
            xu = ut_table[fd].ut;
            xu->io.revents = events[i].events;
            xu->io.fd = fd;
            if (xu->active) {
              query_io(&xu->q, &xu->io);
              r = query_get(&xu->q, &xu->io, &stamp);
              if (r == -1) {
                u_drop(xu);
              } else if (r == 1) {
                u_respond(xu);
              } else {
                query_io(&xu->q, &xu->io);
                fd = xu->q.dt.s1;
                if (fd) {
                  fd--;
                  ut_table[fd].ut = xu;
                  ut_table[fd].client = which;
                }
              }
            } else {
              close(fd);
            }
            break;

          case TCP_CLIENT:
            xt = ut_table[fd].ut;
            xt->io.revents = events[i].events;
            xt->io.fd = xt->tcp;
            if (xt->active) {
              if (xt->io.revents) {
                t_timeout(xt);
                refresh_tcpclient_deadline(xt);
              }
              if (xt->state == 0) {
                query_io(&xt->q, &xt->io);
                r = query_get(&xt->q, &xt->io, &stamp);
                if (r == -1) {
                  t_drop(xt);
                } else if (r == 1) {
                  t_respond(xt);
                } else {
                  query_io(&xt->q, &xt->io);
                  fd = xt->q.dt.s1;
                  if (fd) {
                    fd--;
                    ut_table[fd].ut = xt;
                    ut_table[fd].client = which;
                  }
                }
              } else {
                if (xt->io.revents || taia_less(&xt->timeout,&stamp))
                  t_rw(xt);
              }
            } else {
              close(fd);
            }
            break;

          case INACTIVE:
            break;
        }
      }
    }

    taia_now(&stamp);
    rbtmp = rb_leftmost;
    while (rbtmp) {
      /* process dns_transmit timeouts */
      d = rb_entry(rbtmp, struct dns_transmit, rb_timeout);
      which = d->client;
      xu = d->ctx.u;
      xt = d->ctx.t;
      if (process_timeouts(which, xu, xt, &stamp))
        break;
      rbtmp = rb_next(rbtmp);
    }

    /* process tcp_client timeouts, there is not necessary an active
     * dns_transmit query for every tcpclient */
    list_for_each_entry_safe(tcpcl, tcpcltmp, &tcp_active, list) {
      if (process_timeouts(TCP_CLIENT, NULL, tcpcl, &stamp))
        break;
    }

  }
}

int main()
{
  char *x;
  unsigned int i, j, k;
  unsigned long cachesize;
  static stralloc sa = {0};
  int defer = 10;
  struct rlimit rlim_zero = {
    .rlim_cur = 0,
    .rlim_max = 0
  };

  x = env_get("INTERFACE");
  if (x) scan_ulong(x,&interface);

  x = env_get("IP");
  if (!x)
    strerr_die2x(111,FATAL,"$IP not set");
  if (!ip6_scan(x,myipincoming))
    strerr_die3x(111,FATAL,"unable to parse IP address ",x);

#if 0
  /* if if IP is a mapped-IPv4 address, disable IPv6 functionality */
  /* this is actually a bad idea */
  if (ip6_isv4mapped(myipincoming))
    noipv6 = 1;
#endif

  signal(SIGPIPE, SIG_IGN);

  udp53 = socket_udp6();
  if (udp53 == -1)
    strerr_die2sys(111,FATAL,"unable to create UDP socket: ");
  if (socket_bind6_reuse(udp53,myipincoming,53,interface) == -1)
    strerr_die2sys(111,FATAL,"unable to bind UDP socket: ");

  tcp53 = socket_tcp6();
  if (tcp53 == -1)
    strerr_die2sys(111,FATAL,"unable to create TCP socket: ");
  if (socket_bind6_reuse(tcp53,myipincoming,53,interface) == -1)
    strerr_die2sys(111,FATAL,"unable to bind TCP socket: ");

  dns_random_init();

  droproot(FATAL);

  (void) setrlimit(RLIMIT_NPROC, &rlim_zero);

  socket_tryreservein(udp53,1048576);

  charcrandom_buf(merge_seed, sizeof(merge_seed));

  query_init();

  x = env_get("IPSEND");
  if (!x)
    strerr_die2x(111,FATAL,"$IPSEND not set");
  if (!ip6_scan(x,myipoutgoing))
    strerr_die3x(111,FATAL,"unable to parse IP address ",x);

  x = env_get("CACHESIZE");
  if (!x)
    strerr_die2x(111,FATAL,"$CACHESIZE not set");
  scan_ulong(x,&cachesize);
  if (!cache_init(cachesize))
    strerr_die3x(111,FATAL,"not enough memory for cache of size ",x);

  if (openreadclose("ignoreip",&sa,64) < 0) 
    strerr_die2x(111,FATAL,"trouble reading ignoreip");
  for(j = k = i = 0; i < sa.len; i++)
    if (sa.s[i] == '\n')  {
      sa.s[i] = '\0';
      if (!stralloc_readyplus(&ignoreip,16))
        strerr_die2x(111,FATAL,"out of memory parsing ignoreip");
      if (!ip6_scan(sa.s+k,ignoreip.s+j))
        strerr_die3x(111,FATAL,"unable to parse address in ignoreip ",ignoreip.s+k);
      j += 16;
      k = i + 1;
    }
  ignoreip.len = j;

  if (env_get("HIDETTL"))
    response_hidettl();
  if (env_get("FORWARDONLY"))
    query_forwardonly();
  dns_enable_merge(log_merge);

  if (!roots_init())
    strerr_die2sys(111,FATAL,"unable to read servers: ");

  if (socket_listen(tcp53,1024) == -1)
    strerr_die2sys(111,FATAL,"unable to listen on TCP socket: ");

  setsockopt(tcp53, SOL_TCP, TCP_DEFER_ACCEPT, &defer, sizeof(defer));

  log_startup();
  signal_setup();
  doit();
  return 0;
}

