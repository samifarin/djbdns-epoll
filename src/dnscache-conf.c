#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>

#include "strerr.h"
#include "buffer.h"
#include "helper.h"
#include "taia.h"
#include "str.h"
#include "open.h"
#include "error.h"
#include "exit.h"
#include "generic-conf.h"

#define FATAL "dnscache-conf: fatal: "
#define auto_home "/usr/local"

int noipv6;

void usage(void)
{
  strerr_die1x(100,"dnscache-conf: usage: dnscache-conf acct logacct /dnscache [ myip ]");
}

int fdrootservers;
char rootserversbuf[64];
buffer ssrootservers;

char *dir;
char *user;
char *loguser;
struct passwd *pw;
const char *myip;

int main(int argc,char **argv)
{
  user = argv[1];
  if (!user) usage();
  loguser = argv[2];
  if (!loguser) usage();
  dir = argv[3];
  if (!dir) usage();
  if (dir[0] != '/') usage();
  myip = argv[4];
  if (!myip) myip = "127.0.0.1";

  pw = getpwnam(loguser);
  if (!pw)
    strerr_die3x(111,FATAL,"unknown account ",loguser);

  if (chdir(auto_home) == -1)
    strerr_die4sys(111,FATAL,"unable to switch to ",auto_home,": ");

  fdrootservers = open_read("/dnsroots.local");
  if (fdrootservers == -1) {
    if (errno != error_noent)
      strerr_die2sys(111,FATAL,"unable to open /etc/dnsroots.local: ");
    fdrootservers = open_read("/etc/dnsroots.global");
    if (fdrootservers == -1)
      strerr_die2sys(111,FATAL,"unable to open /etc/dnsroots.global: ");
  }

  init(dir,FATAL);

  makedir("log");
  perm(02755);
  makedir("log/main");
  owner(pw->pw_uid,pw->pw_gid);
  perm(02755);
  start("log/status"); finish();
  owner(pw->pw_uid,pw->pw_gid);
  perm(0644);
  makedir("env");
  perm(02755);
  start("env/ROOT"); outs(dir); outs("/root\n"); finish();
  perm(0644);
  start("env/IP"); outs(myip); outs("\n"); finish();
  perm(0644);
  start("env/IPSEND"); outs("0.0.0.0\n"); finish();
  perm(0644);
  start("env/CACHESIZE"); outs("32000000\n"); finish();
  perm(0644);
  start("env/DATALIMIT"); outs("80000000\n"); finish();
  perm(0644);
  start("run");
  outs("#!/bin/sh\nulimit -n 11111\nexec 2>&1\nexec envdir ./env sh -c '\n  exec envuidgid "); outs(user);
  outs(" ");
  outs(auto_home); outs("/bin/dnscache\n'\n"); finish();
   perm(0755);
  start("log/run");
  outs("#!/bin/sh\nexec setuidgid "); outs(loguser);
  outs(" multilog t ./main\n"); finish();
  perm(0755);
  makedir("root");
  perm(02755);
  makedir("root/servers");
  perm(02755);
  start("root/servers/@");
  buffer_init(&ssrootservers,buffer_unixread,fdrootservers,rootserversbuf,sizeof rootserversbuf);
  copyfrom(&ssrootservers);
  finish();
  perm(0644);

  _exit(0);
}
