#ifndef TAI_H
#define TAI_H

#include <time.h>

#include "helper.h"

struct tai {
  uint64 x;
} ;

#define tai_unix(t,u) ((void) ((t)->x = 4611686018427387914ULL + (uint64) (u)))

static inline void tai_now(struct tai *t)
{
  tai_unix(t,time((time_t *) 0));
}

#define tai_approx(t) ((double) ((t)->x))

static inline void tai_add(struct tai *t,const struct tai *u,const struct tai *v)
{
  t->x = u->x + v->x;
}

static inline void tai_sub(struct tai *t,const struct tai *u,const struct tai *v)
{
  t->x = u->x - v->x;
}

#define tai_less(t,u) ((t)->x < (u)->x)

#define TAI_PACK 8

static inline void tai_pack(char *s,const struct tai *t)
{
  uint64 x;

  x = t->x;
  s[7] = x & 255; x >>= 8;
  s[6] = x & 255; x >>= 8;
  s[5] = x & 255; x >>= 8;
  s[4] = x & 255; x >>= 8;
  s[3] = x & 255; x >>= 8;
  s[2] = x & 255; x >>= 8;
  s[1] = x & 255; x >>= 8;
  s[0] = x;
}

static inline void tai_unpack(const char *s,struct tai *t)
{
  uint64 x;

  x = (unsigned char) s[0];
  x <<= 8; x += (unsigned char) s[1];
  x <<= 8; x += (unsigned char) s[2];
  x <<= 8; x += (unsigned char) s[3];
  x <<= 8; x += (unsigned char) s[4];
  x <<= 8; x += (unsigned char) s[5];
  x <<= 8; x += (unsigned char) s[6];
  x <<= 8; x += (unsigned char) s[7];
  t->x = x;
}

static inline void tai_uint(struct tai *t,unsigned int u)
{
  t->x = u;
}

#endif
