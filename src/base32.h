#ifndef _BASE32_H_
#define _BASE32_H_

#include <stddef.h>
#include <stdint.h>

extern const char base32_digits[];
extern void base32_encodebytes(char *out, const char *in, size_t len);
extern void base32_encodekey(char *out, const char *key);
extern unsigned int base32_decode(char *out,const char *in,size_t len,int mode);

static inline size_t base32_bytessize(size_t len)
{
  if (len > ((SIZE_MAX/8) - 4)) return SIZE_MAX;
  len = (8 * len + 4) / 5;
  return len + (len + 49) / 50;
}

#endif
