#include "socket.h"
#include "error.h"

int socket_tcp6(void)
{
  int s;

  if (noipv6) goto compat;
  s = socket(PF_INET6, SOCK_STREAM | SOCK_NONBLOCK, 0);
  if (s == -1) {
    if (errno == EINVAL || errno == EAFNOSUPPORT || errno == EPROTONOSUPPORT || errno == EPFNOSUPPORT) {
compat:
      s=socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
      noipv6=1;
      if (s==-1) return -1;
    } else
    return -1;
  }
#ifdef IPV6_V6ONLY
  {
    int zero=0; /* net.ipv6.bindv6only */
    setsockopt(s,IPPROTO_IPV6,IPV6_V6ONLY,(void*)&zero,sizeof(zero));
  }
#endif
  return s;
}

