#ifndef IOPAUSE_H
#define IOPAUSE_H

#include <sys/epoll.h>
#include <sys/types.h>

typedef struct {
  int fd;                     /* File descriptor to poll.  */
  short int events;           /* Types of events poller cares about.  */
  short int revents;          /* Types of events that actually occurred.  */
} iopause_fd;

#define IOPAUSE_READ EPOLLIN
#define IOPAUSE_WRITE EPOLLOUT

#include "taia.h"

extern int iopause(int, struct epoll_event*,int,struct taia *,struct taia *);

extern void iopause_poll(iopause_fd *x, unsigned int len, struct taia *deadline, struct taia *stamp);

#endif
