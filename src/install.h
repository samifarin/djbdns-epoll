#ifndef _INSTALL_H_
#define _INSTALL_H_

void h(const char *home,int uid,int gid,int mode);
void d(const char *home,char *subdir,int uid,int gid,int mode);
void c(const char *home,char *subdir,char *file,int uid,int gid,int mode);
void z(const char *home,char *subdir,char* file,int len,int uid,int gid,int mode);

#endif
